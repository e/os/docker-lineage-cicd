#!/bin/bash

# Check if shfmt is installed
if ! command -v shfmt &>/dev/null; then
  echo "shfmt is not installed. Please install shfmt (curl -sS https://webi.sh/shfmt | sh) to fix formatting."
  exit 0
fi

# Get the directory of the script
SCRIPT_DIR="$(cd "$(dirname "$0")"/ && pwd)"

# Define a function to format shell scripts
format_scripts() {
  local dir="$1"
  # Iterate over each shell script file in the directory
  while IFS= read -r -d '' script_file; do
    echo "Formatting $script_file ..."
    shfmt -w -i 2 "$script_file"
    git add "$script_file"
  done < <(find "$dir" -type f -name '*.sh' -print0)
}

# Format scripts in the current directory
format_scripts "$SCRIPT_DIR"

# Recursively format scripts in subdirectories
while IFS= read -r -d '' subdir; do
  format_scripts "$subdir"
done < <(find "$SCRIPT_DIR" -type d -print0)

echo "Formatting complete!"
