#!/bin/bash

# Docker build script
# Copyright (c) 2017 Julian Xhokaxhiu
# Copyright (C) 2017-2018 Nicola Corna <nicola@corna.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# cd to working directory
cd "$SRC_DIR" || return 1

# Determine the number of parallel jobs to use for repo sync
nproc_value=$(nproc --all)
JOBS=$((nproc_value > 10 ? 10 : nproc_value))

if [ -f /root/userscripts/begin.sh ]; then
  echo ">> [$(date)] Running begin.sh"
  /root/userscripts/begin.sh
fi

# If requested, clean the OUT dir in order to avoid clutter
if [ "$CLEAN_OUTDIR" = true ]; then
  echo ">> [$(date)] Cleaning '$ZIP_DIR'"
  rm -rf "${ZIP_DIR:?}/*"
fi

# Define a function to extract images based on the provided header and config file
extract_images() {
  local header="$1"
  local config_file="$2"
  if [ -f "$config_file" ]; then
    awk -v header="$header" '$0 ~ header {flag=1; print; next} /^$/{flag=0}flag' "$config_file" | tr -d '\r\n' | sed 's/\\//g' | grep -oP "(?<=${header} ).*"
  fi
}

clean_up() {
  # Remove old zips and logs
  if [ "$DELETE_OLD_ZIPS" -gt "0" ]; then
    if [ "$ZIP_SUBDIR" = true ]; then
      /usr/bin/python /root/clean_up.py -n "$DELETE_OLD_ZIPS" -V "$e_ver" -N 1 "$ZIP_DIR/$zipsubdir"
    else
      /usr/bin/python /root/clean_up.py -n "$DELETE_OLD_ZIPS" -V "$e_ver" -N 1 -c "${DEVICE}" "$ZIP_DIR"
    fi
  fi
  if [ "$DELETE_OLD_LOGS" -gt "0" ]; then
    if [ "$LOGS_SUBDIR" = true ]; then
      /usr/bin/python /root/clean_up.py -n "$DELETE_OLD_LOGS" -V "$e_ver" -N 1 "$LOGS_DIR/$logsubdir"
    else
      /usr/bin/python /root/clean_up.py -n "$DELETE_OLD_LOGS" -V "$e_ver" -N 1 -c "${DEVICE}" "$LOGS_DIR"
    fi
  fi
  if [ -f /root/userscripts/post-build.sh ]; then
    echo ">> [$(date)] Running post-build.sh for ${DEVICE}"
    /root/userscripts/post-build.sh "${DEVICE}" "$build_successful"
  fi
  echo ">> [$(date)] Finishing build for ${DEVICE}"

  if [ "$CLEAN_AFTER_BUILD" = true ]; then
    echo ">> [$(date)] Cleaning source dir for device ${DEVICE}"
    cd "$source_dir" || return 1
    mka clean
  fi
}

exit_script() {
  if [ "$DELETE_OLD_LOGS" -gt "0" ]; then
    find "$LOGS_DIR" -maxdepth 1 -name "repo-*.log" | sort | head -n -"$DELETE_OLD_LOGS" | xargs -r rm
  fi

  if [ -f /root/userscripts/end.sh ]; then
    echo ">> [$(date)] Running end.sh"
    /root/userscripts/end.sh
  fi

  if [ "$build_successful" = false ] || [ "$sync_successful" = false ]; then
    exit 1
  fi
}

sync_successful=true

use_openjdk_from_ubuntu=false
branch_dir=$(sed -E 's/^v[0-9](\.[0-9]*){0,2}(-(beta|alpha|rc)(\.[0-9]*){0,1}){0,1}-(nougat|oreo|pie|q|r|s|t|u|a14|a15)(-[a-zA-Z0-9_]*)*$/\5/' <<<"${BRANCH_NAME}")
branch_dir=${branch_dir^^}

if [ -n "${BRANCH_NAME}" ] && [ -n "${DEVICE}" ]; then
  vendor=lineage
  regex_part1="^v[0-9](\.[0-9]*){0,2}(-(beta|alpha|rc)(\.[0-9]*){0,1}){0,1}-("
  regex_part2=")(-[a-zA-Z0-9_]*)*$"
  if [[ "${BRANCH_NAME}" =~ $regex_part1"nougat"$regex_part2 ]]; then
    vendor="cm"
    themuppets_branch="cm-14.1"
    android_version="7.1.2"
    use_openjdk_from_ubuntu=true
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"oreo"$regex_part2 ]]; then
    themuppets_branch="lineage-15.1"
    android_version="8.1"
    use_openjdk_from_ubuntu=true
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"pie"$regex_part2 ]]; then
    themuppets_branch="lineage-16.0"
    android_version="9"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"q"$regex_part2 ]]; then
    themuppets_branch="lineage-17.1"
    android_version="10"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"r"$regex_part2 ]]; then
    themuppets_branch="lineage-18.1"
    android_version="11"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"s"$regex_part2 ]]; then
    themuppets_branch="lineage-19.1"
    android_version="12"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"t"$regex_part2 ]]; then
    themuppets_branch="lineage-20.0"
    android_version="13"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"u"$regex_part2 ]]; then
    themuppets_branch="lineage-21.0"
    android_version="14"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"a14"$regex_part2 || "${BRANCH_NAME}" =~ "a14" ]]; then
    themuppets_branch="lineage-21.0"
    android_version="14"
  elif [[ "${BRANCH_NAME}" =~ $regex_part1"a15"$regex_part2 || "${BRANCH_NAME}" =~ "a15" ]]; then
    themuppets_branch="lineage-22.1"
    android_version="15"
  else
    echo ">> [$(date)] Building branch $BRANCH_NAME is not (yet) supported"
    exit 1
  fi

  if [[ "${KEEP_LOCAL_MANIFEST_BRANCH}" = true ]]; then
    themuppets_branch=$BRANCH_NAME
  fi

  android_version_major=$(cut -d '.' -f 1 <<<$android_version)

  mkdir -p "$SRC_DIR/$branch_dir"
  cd "$SRC_DIR/$branch_dir" || return 1

  mkdir -p "$CCACHE_BASE_DIR/$branch_dir"
  export CCACHE_DIR="$CCACHE_BASE_DIR/$branch_dir"

  echo ">> [$(date)] Setting up CCache, CCACHE_DIR=${CCACHE_DIR} CCACHE_SIZE=${CCACHE_SIZE}"

  echo ">> [$(date)] Branch:  ${BRANCH_NAME}"
  echo ">> [$(date)] Device: ${DEVICE}"

  # Remove previous changes of vendor/cm, vendor/lineage and frameworks/base (if they exist)
  for path in "vendor/cm" "vendor/lineage" "frameworks/base"; do
    if [ -d "$path" ]; then
      cd "$path" || return 1
      git reset -q --hard
      git clean -q -fd
      cd "$SRC_DIR/$branch_dir" || return 1
    fi
  done

  echo ">> [$(date)] (Re)initializing branch repository"

  TAG_PREFIX=""

  # Fetch all tags and check for an exact match
  TAG_MATCH=$(curl -s --fail https://gitlab.e.foundation/api/v4/projects/659/repository/tags | jq -r ".[].name" | grep -Fx "$BRANCH_NAME")

  if [ -n "$TAG_MATCH" ]; then
    echo ">> [$(date)] Branch name $BRANCH_NAME is a tag on e/os/releases, prefixing with refs/tags/ for 'repo init'"
    TAG_PREFIX="refs/tags/"
  fi

  if [ -n ${REPO_INIT_DEPTH} ] && [ ${REPO_INIT_DEPTH} -gt 0 ]; then
    REPO_INIT_PARAM="--depth ${REPO_INIT_DEPTH}"
  fi

  # Reset the default manifest if it exists
  if [ -f ".repo/manifests/default.xml" ]; then
    cd .repo/manifests/
    git reset --hard HEAD
    git clean -fd
    cd ../..
  fi

  # Set the REPO_GROUPS variable based on the android_version_major
  if [ "$android_version_major" -ge 14 ]; then
    REPO_GROUPS="-g default,-darwin,-muppets,muppets_$DEVICE"
  else
    REPO_GROUPS=""
  fi

  # Repo init source
  echo ">> [$(date)] Running: repo init $REPO_INIT_PARAM -u \"$REPO\" -b \"${TAG_PREFIX}${BRANCH_NAME}\" $REPO_GROUPS"
  yes | repo init $REPO_INIT_PARAM -u "$REPO" -b "${TAG_PREFIX}${BRANCH_NAME}" $REPO_GROUPS

  if [ "$REPO_CUSTOM_MANIFEST" != false ]; then
    wget -O .repo/manifests/default.xml $REPO_CUSTOM_MANIFEST
  fi

  # Copy local manifests to the appropriate folder in order take them into consideration
  echo ">> [$(date)] Copying '$LMANIFEST_DIR/*.xml' to '.repo/local_manifests/'"
  mkdir -p .repo/local_manifests
  rsync -a --delete --include '*.xml' --exclude '*' "$LMANIFEST_DIR/" .repo/local_manifests/

  rm -f .repo/local_manifests/proprietary.xml
  if [ "$INCLUDE_PROPRIETARY" = true ]; then
    wget -q -O .repo/local_manifests/proprietary.xml "https://raw.githubusercontent.com/TheMuppets/manifests/$themuppets_branch/muppets.xml"
    /root/build_manifest.py --remote "https://gitlab.com" --remotename "gitlab_https" \
      "https://gitlab.com/the-muppets/manifest/raw/$themuppets_branch/muppets.xml" .repo/local_manifests/proprietary_gitlab.xml

  fi

  echo ">> [$(date)] Syncing branch repository"
  builddate=$(date +%Y%m%d)
  repo_out=$(repo sync -c -j"$JOBS" --force-sync 2>&1 >/dev/null)
  repo_status=$?
  echo -e $repo_out

  if [ "$repo_status" != "0" ]; then
    if [ -f /root/userscripts/clean.sh ]; then
      if [[ "$repo_out" == *"Failing repos:"* ]]; then
        list_line=$(echo -e $repo_out | sed 's/.*Failing repos: //')
      fi
      if [[ "$repo_out" == *"Cannot remove project"* ]]; then
        list_line=$(echo -e $repo_out | grep "Cannot remove project" | sed -e 's/.*error: \(.*\): Cannot.*/\1/')
      fi
      echo ">> [$(date)] Running clean.sh"
      /root/userscripts/clean.sh $list_line
      if ! repo sync -c --force-sync; then
        sync_successful=false
      fi
    else
      sync_successful=false
    fi
  fi

  if [ "$sync_successful" = true ]; then
    repo forall -c 'git lfs pull'
  fi

  if [ ! -d "vendor/$vendor" ]; then
    echo ">> [$(date)] Missing \"vendor/$vendor\", aborting"
    exit 1
  fi

  config_file="vendor/lineage/config/version.mk"
  if [ ! -f "$config_file" ]; then
    # If version.mk doesn't exist, use common.mk
    config_file="vendor/lineage/config/common.mk"
  fi

  # Extract version information
  PRODUCT_VERSION_MAJOR=$(grep -oP 'PRODUCT_VERSION_MAJOR = \K.*' "$config_file")
  PRODUCT_VERSION_MINOR=$(grep -oP 'PRODUCT_VERSION_MINOR = \K.*' "$config_file")
  PRODUCT_VERSION_MAINTENANCE=$(grep -oP 'PRODUCT_VERSION_MAINTENANCE := \K.*' "$config_file")
  PRODUCT_PRERELEASE_STRING=$(grep -oP 'PRODUCT_PRERELEASE_STRING := \K.*' "$config_file")

  # Check if PRODUCT_VERSION_MAINTENANCE is not equal to 0
  e_ver="$PRODUCT_VERSION_MAJOR.$PRODUCT_VERSION_MINOR$PRODUCT_PRERELEASE_STRING"
  if [ "$PRODUCT_VERSION_MAINTENANCE" != "0" ]; then
    e_ver="$PRODUCT_VERSION_MAJOR.$PRODUCT_VERSION_MINOR.$PRODUCT_VERSION_MAINTENANCE$PRODUCT_PRERELEASE_STRING"
  fi

  if [ "$SIGN_BUILDS" = true ]; then
    echo ">> [$(date)] Adding keys path ($KEYS_DIR)"
    # Soong (Android 9+) complains if the signing keys are outside the build path
    ln -sf "$KEYS_DIR" user-keys
    if [ "$android_version_major" -lt "10" ]; then
      sed -i "1s;^;PRODUCT_DEFAULT_DEV_CERTIFICATE := user-keys/releasekey\nPRODUCT_OTA_PUBLIC_KEYS := user-keys/releasekey\nPRODUCT_EXTRA_RECOVERY_KEYS := user-keys/releasekey\n\n;" "vendor/$vendor/config/common.mk"
    elif [ "$android_version_major" -lt "12" ]; then
      sed -i "1s;^;PRODUCT_DEFAULT_DEV_CERTIFICATE := user-keys/releasekey\nPRODUCT_OTA_PUBLIC_KEYS := user-keys/releasekey\n\n;" "vendor/$vendor/config/common.mk"
    fi
  fi

  # Prepare the environment
  echo ">> [$(date)] Preparing build environment"
  source build/envsetup.sh >/dev/null

  if [ -f /root/userscripts/before.sh ]; then
    echo ">> [$(date)] Running before.sh"
    /root/userscripts/before.sh
  fi

  build_device=true
  if [ -n "${DEVICE}" ]; then

    currentdate=$(date +%Y%m%d)
    if [ "$builddate" != "$currentdate" ]; then
      # Sync the source code
      builddate=$currentdate

      echo ">> [$(date)] Syncing branch repository"
      cd "$SRC_DIR/$branch_dir" || return 1

      if ! repo sync -c -j"$JOBS" --force-sync; then
        sync_successful=false
        build_device=false
      fi
    fi

    source_dir="$SRC_DIR/$branch_dir"
    cd "$source_dir" || return 1

    if [ "$ZIP_SUBDIR" = true ]; then
      zipsubdir=${DEVICE}
      mkdir -p "$ZIP_DIR/$zipsubdir"
    else
      zipsubdir=
    fi
    if [ "$LOGS_SUBDIR" = true ]; then
      logsubdir=${DEVICE}
      mkdir -p "$LOGS_DIR/$logsubdir"
    else
      logsubdir=
    fi

    if [ -f /root/userscripts/pre-build.sh ]; then
      echo ">> [$(date)] Running pre-build.sh for ${DEVICE}"

      if ! /root/userscripts/pre-build.sh "${DEVICE}"; then
        build_device=false
      fi
    fi

    if [ "$build_device" = false ]; then
      echo ">> [$(date)] No build for ${DEVICE}"
    fi

    if [ "$use_openjdk_from_ubuntu" = true ]; then
      update-java-alternatives -s java-1.8.0-openjdk-amd64
    fi

    # Start the build
    echo ">> [$(date)] Starting build for ${DEVICE}, ${BRANCH_NAME} branch"
    build_successful=false
    echo ">> [$(date)] ANDROID_JACK_VM_ARGS=${ANDROID_JACK_VM_ARGS}"

    BUILD_TYPE=userdebug

    if [ "${ENG_BUILD}" = true ]; then
      BUILD_TYPE=eng
    elif [ "${USER_BUILD}" = true ]; then
      BUILD_TYPE=user
    fi

    BRUNCH_DEVICE=lineage_${DEVICE}-${BUILD_TYPE}

    if [[ "${BRANCH_NAME}" = *"rc"* ]] || [[ "${BRANCH_NAME}" = *"alpha"* ]] || [[ "${BRANCH_NAME}" == *"beta"* ]]; then
      PRODUCT_PRERELEASE=$(echo ${BRANCH_NAME} | sed -E 's/v[0-9]*\.[0-9]*(\.[0-9]*)?-(beta|alpha|rc).*/\2/')
      export PRODUCT_PRERELEASE
      if [[ "${BRANCH_NAME}" = *"rc."* ]] || [[ "${BRANCH_NAME}" = *"alpha."* ]] || [[ "${BRANCH_NAME}" == *"beta."* ]]; then
        PRODUCT_PRERELEASE_VERSION=$(echo ${BRANCH_NAME} | sed -E 's/v[0-9]*\.[0-9]*(\.[0-9]*)?-.*.\.([0-9]*).*/\2/')
        export PRODUCT_PRERELEASE_VERSION
      fi
    fi

    build_success=false
    breakfast_device() {
      if [ "$android_version_major" -gt "13" ]; then
        breakfast "${DEVICE}" "${BUILD_TYPE}"
      else
        breakfast "${BRUNCH_DEVICE}"
      fi
    }

    if [ "${BUILD_ONLY_SYSTEMIMAGE}" = true ]; then
      breakfast_device
      if make systemimage; then
        build_success=true
      fi
    elif [ "${IS_EMULATOR}" = true ]; then
      if breakfast_device; then
        if [ "$android_version_major" -lt "14" ]; then
          if mka sdk_addon; then
            build_success=true
          fi
        elif mka && mka emu_img_zip; then
          build_success=true
        fi
      fi
    elif [ "$android_version_major" -lt "12" ] || [ "${RELEASE_TYPE}" = "test" ]; then
      if [ "${BUILD_SUPER_IMAGE}" = true ]; then
        if breakfast_device && mka bacon superimage; then
          build_success=true
        fi
      elif brunch "${DEVICE}" "${BUILD_TYPE}"; then
        build_success=true
      fi
    elif breakfast_device; then
      # Apply patch for vendor prebuilt devices.
      if [ "${DEVICE}" = "one" ] || [ "${DEVICE}" = "two" ]; then
        echo ">> [$(date)] Applying patch for prebuilt vendor device"
        cd build/make
        git am /root/0001-releasetools-support-prebuilt-vendor.img.patch
        cd ../../
      fi

      mka target-files-package otatools
      echo ">> [$(date)] Starting signing target-files-package"

      # Set the target files name
      BUILD_NUMBER=$(date -u +%Y%m%d)${CI_PIPELINE_ID}
      TARGET_FILES=lineage_$DEVICE-target_files-$BUILD_NUMBER.zip
      E_BRANCH_NAME=$e_ver-${branch_dir,,}
      if [[ "${BRANCH_NAME}" = *"rc"* ]] || [[ "${BRANCH_NAME}" = *"alpha"* ]] || [[ "${BRANCH_NAME}" == *"beta"* ]]; then
        E_BRANCH_NAME=$(sed 's/^v//' <<<"${BRANCH_NAME}")
      fi
      E_VERSION=e-$E_BRANCH_NAME-$BUILD_NUMBER-$RELEASE_TYPE-${DEVICE}.zip

      # Remove release-keys from build as its still using test-keys.
      if [ "$SIGN_BUILDS" != true ]; then
        SIGN_TARGETS=(-t "+test-keys,-release-keys")
      else
        SIGN_TARGETS=(-o -d "user-keys")
      fi

      if [ "$android_version_major" -ge 12 ] && [ "$SIGN_BUILDS" = true ]; then
        APEX_PACKAGE_LIST=$(cat "/root/apex.list")
        for PACKAGE in $APEX_PACKAGE_LIST; do
          if [ -f "user-keys/$PACKAGE.pem" ] &&
            [ -f "user-keys/$PACKAGE.x509.pem" ] &&
            [ -f "user-keys/$PACKAGE.pk8" ]; then
            SIGN_TARGETS+=(--extra_apks "$PACKAGE.apex=user-keys/$PACKAGE"
              --extra_apex_payload_key "$PACKAGE.apex=user-keys/$PACKAGE.pem")
          fi
        done

        APEXAPK_PACKAGE_LIST=$(cat "/root/apex_apk.list")
        for PACKAGE in $APEXAPK_PACKAGE_LIST; do
          SIGN_TARGETS+=(--extra_apks "$PACKAGE.apk=user-keys/releasekey")
        done
      fi

      if [ "$(find $OUT/obj/PACKAGING/target_files_intermediates/ -name *-target_files*.zip -print -quit)" ]; then
        sign_target_files_apks "${SIGN_TARGETS[@]}" \
          $OUT/obj/PACKAGING/target_files_intermediates/*-target_files*.zip "$OUT/$TARGET_FILES"

        # Generate OTA zip
        echo ">> [$(date)] Generating OTA zip from target-files-package"
        if [ "$SIGN_BUILDS" = true ]; then
          SIGN_KEY=(-k "user-keys/releasekey")
        fi
        ota_from_target_files --backup=true "${SIGN_KEY[@]}" "$OUT/$TARGET_FILES" \
          "$OUT/$E_VERSION"

        if [ ! -f "$OUT/$E_VERSION" ]; then
          clean_up
          exit_script
        fi

        # Generate fastboot package zip (Doesn't include firmware)
        # Flash: fastboot update update.zip
        FASTBOOT_PACKAGE="FASTBOOT-$E_VERSION"
        echo ">> [$(date)] Generating fastboot zip from target-files-package"
        img_from_target_files "$OUT/$TARGET_FILES" "$OUT/$FASTBOOT_PACKAGE"

        if [ ! -f "$OUT/$FASTBOOT_PACKAGE" ]; then
          clean_up
          exit_script
        fi

        IMAGES=(
          "boot"
          "dtbo"
          "init_boot"
          "odm"
          "odm_dlkm"
          "product"
          "pvmfw"
          "recovery"
          "super"
          "super_empty"
          "system"
          "system_dlkm"
          "system_ext"
          "vendor"
          "vendor_boot"
          "vendor_dlkm"
          "vendor_kernel_boot"
          "vbmeta"
          "vbmeta_system"
          "vbmeta_vendor"
        )

        for i in "${!IMAGES[@]}"; do
          if unzip -l "$OUT/$FASTBOOT_PACKAGE" | grep -q "${IMAGES[i]}.img"; then
            unzip -o -j -q "$OUT/$FASTBOOT_PACKAGE" "${IMAGES[i]}.img" -d "$OUT"
          fi
        done

        IMG_FLASH_PATH="$source_dir/vendor/eos/flash"
        IMG_FIRMWARE_PATH="$source_dir/vendor/firmware/${DEVICE}/radio"
        QFIL_FLASH_PATH="$source_dir/vendor/murena-flash"

        if [ ! -d "$IMG_FIRMWARE_PATH" ]; then
          # If it doesn't exist, use an alternative path, used in lineage-21
          IMG_FIRMWARE_VENDOR_PATH=$(repo list | grep "proprietary_vendor_.*_${DEVICE}" | cut -d : -f 1 | xargs)
          if [ -n "$IMG_FIRMWARE_VENDOR_PATH" ] && [ -d "$source_dir/$IMG_FIRMWARE_VENDOR_PATH" ]; then
            IMG_FIRMWARE_PATH="$source_dir/$IMG_FIRMWARE_VENDOR_PATH/radio"
            echo ">> INFO: \$IMG_FIRMWARE_PATH: $IMG_FIRMWARE_PATH"
          fi
        fi

        QFIL_CONFIG=$QFIL_FLASH_PATH/${DEVICE}/config.mk
        IMG_CONFIG=$IMG_FLASH_PATH/${DEVICE}/config.mk

        QFIL_IMAGES=$(extract_images "HLOS_QFIL_TARGET :=" "$QFIL_CONFIG")
        IMG_IMAGES=$(extract_images "HLOS_IMAGES_TARGET :=" "$IMG_CONFIG")

        IMAGE_PACKAGE_NAME=IMG-${E_VERSION}
        QFIL_IMAGE_PACKAGE_NAME=QFIL-${E_VERSION}

        if echo "$QFIL_IMAGES $IMG_IMAGES" | grep -q "super.img"; then
          build_super_image "$OUT/$TARGET_FILES" "$OUT/super.img"
        fi

        if echo "$QFIL_IMAGES $IMG_IMAGES" | grep -q "userdata.img"; then
          mka userdataimage
        fi

        if echo "$QFIL_IMAGES $IMG_IMAGES" | grep -q "cache.img"; then
          mka cacheimage
        fi

        # Create a temp folder for creating IMG-e*
        mkdir -p $OUT/IMG

        if [ -f "$IMG_FLASH_PATH/${DEVICE}/flash_${DEVICE}_factory.sh" ]; then
          # Copy required images for IMG-e*
          for IMAGE in $IMG_IMAGES; do
            cp $OUT/$IMAGE $OUT/IMG/$IMAGE
          done

          # Copy required images for IMG-e*
          if [ -d "$IMG_FIRMWARE_PATH" ]; then
            cp -rf "$IMG_FIRMWARE_PATH/"* $OUT/IMG/
          fi

          # Copy required fastboot tools for IMG-e*
          cp -r $IMG_FLASH_PATH/fastboot/bin-msys $OUT/IMG/bin-msys
          cp -r $IMG_FLASH_PATH/fastboot/bin-linux-x86 $OUT/IMG/bin-linux-x86
          cp -r $IMG_FLASH_PATH/fastboot/bin-darwin $OUT/IMG/bin-darwin
          cp $IMG_FLASH_PATH/factory.common $OUT/IMG/factory.common
          cp $IMG_FLASH_PATH/${DEVICE}/flash_${DEVICE}_factory.sh $OUT/IMG/flash_${DEVICE}_factory.sh

          echo ">> [$(date)] Generating IMG zip"
          cd $OUT/IMG
          zip -r $OUT/$IMAGE_PACKAGE_NAME *

          rm -rf $OUT/IMG/bin-msys $OUT/IMG/bin-linux-x86 $OUT/IMG/bin-darwin $OUT/IMG/factory.common $OUT/IMG/flash_${DEVICE}_factory.sh $OUT/IMG/fastboot_update.zip
          cd "$source_dir"
        fi

        if [ -f "$QFIL_FLASH_PATH/${DEVICE}/configs/rawprogram0.xml" ] && [ ! -z "$QFIL_IMAGES" ]; then
          # Copy required images for IMG-e*
          for IMAGE in $QFIL_IMAGES; do
            cp $OUT/$IMAGE $OUT/IMG/$IMAGE
          done

          cp -R $QFIL_FLASH_PATH/${DEVICE}/configs/* $OUT/IMG/
          cp -R $QFIL_FLASH_PATH/${DEVICE}/prebuilts/* $OUT/IMG/
          python2 $QFIL_FLASH_PATH/checksparse.py -i $OUT/IMG/rawprogram0.xml -s $OUT/IMG -t $OUT/IMG

          echo ">> [$(date)] Generating QFIL zip"
          cd $OUT/IMG
          zip -r $OUT/$QFIL_IMAGE_PACKAGE_NAME *

          cd "$source_dir"
        fi

        cd $OUT
        for PACKAGE_NAME in $(ls *${E_VERSION}); do
          md5sum $PACKAGE_NAME >$PACKAGE_NAME.md5sum
          sha256sum $PACKAGE_NAME >$PACKAGE_NAME.sha256sum
        done
        cd "$source_dir"

        rm -rf $OUT/IMG
        build_success=true
      else
        echo ">> [$(date)] Unable to find $TARGET_FILES"
      fi
    fi

    if [ "$build_success" = true ]; then
      currentdate=$(date +%Y%m%d)
      if [ "$builddate" != "$currentdate" ]; then
        find "${OUT}" -maxdepth 1 -name "e-*-$currentdate-*.zip*" -type f -exec sh /root/fix_build_date.sh {} "$currentdate" "$builddate" \;
      fi

      IMG_OUT=$OUT
      if [ "$android_version_major" -lt "12" ]; then
        IMG_OUT=obj/PACKAGING/target_files_intermediates/lineage_*/IMAGES
      fi

      # Move produced ZIP files to the main OUT directory
      echo ">> [$(date)] Moving build artifacts for ${DEVICE} to '$ZIP_DIR/$zipsubdir'"
      cd "${OUT}" || return 1
      for build in $(ls e-*.zip); do
        if [ "$android_version_major" -lt "12" ]; then
          sha256sum "$build" >"$ZIP_DIR/$zipsubdir/$build.sha256sum"
        fi
        find . -maxdepth 1 \( -name 'e-*.zip*' -o -name 'IMG-e*.zip*' -o -name 'QFIL-e*.zip*' \) -type f -exec mv {} "$ZIP_DIR/$zipsubdir/" \;

        SKIP_DYNAMIC_IMAGES="odm.img product.img system.img system_ext.img vendor.img"
        if [ "$BACKUP_IMG" = true ]; then
          if [ "$BUILD_FLASH_PACKAGE" = true ]; then
            echo "backing up flash tools"
            git clone https://gitlab.e.foundation/e/os/flash_tools
            cd flash_tools
            zip -r "$ZIP_DIR/$zipsubdir/IMG-$build" bin*
            zip "$ZIP_DIR/$zipsubdir/IMG-$build" flash_${DEVICE}_factory.sh
            cd ..
          fi
          if [ "$BUILD_SUPER_IMAGE" = true ]; then
            find . -maxdepth 1 \( -name '*.img' -o -name '*_scatter.txt' -o -name '*.bin' \) -type f $(printf "! -name %s " $(echo "$SKIP_DYNAMIC_IMAGES")) -exec zip "$ZIP_DIR/$zipsubdir/IMG-$build" {} \;
          elif [ "$SPARSE_PREBUILT_VENDOR_IMAGE" = true ]; then
            echo "Sparsing prebuilt vendor image"
            img2simg vendor.img vendor-sparsed.img || return 1
            find . -maxdepth 1 \( -name '*.img' -o -name '*_scatter.txt' -o -name '*.bin' \) -type f ! -name vendor.img -exec zip "$ZIP_DIR/$zipsubdir/IMG-$build" {} \;
          else
            find . -maxdepth 1 \( -name '*.img' -o -name '*_scatter.txt' -o -name '*.bin' \) -type f -exec zip "$ZIP_DIR/$zipsubdir/IMG-$build" {} \;
          fi
          cd "$ZIP_DIR/$zipsubdir" || return 1
          sha256sum "IMG-$build" >"IMG-$build.sha256sum"
          md5sum "IMG-$build" >"IMG-$build.md5sum"
          cd "${OUT}" || return 1
        fi
        if [ "$BACKUP_INTERMEDIATE_SYSTEM_IMG" = true ]; then
          mv $IMG_OUT/system.img ./
          zip "$ZIP_DIR/$zipsubdir/IMG-$build" system.img
          cd $ZIP_DIR/$zipsubdir
          sha256sum "IMG-$build" >"IMG-$build.sha256sum"
          md5sum "IMG-$build" >"IMG-$build.md5sum"
          cd "${OUT}" || return 1
        fi

        if [ "$RECOVERY_IMG" = true ]; then
          BOOT_IMG_NAME="boot-${build%.*}.img"
          DTBO_IMG_NAME="dtbo-${build%.*}.img"
          SUPER_EMPTY_IMG_NAME="super_empty-${build%.*}.img"
          VBMETA_IMG_NAME="vbmeta-${build%.*}.img"
          VENDOR_BOOT_IMG_NAME="vendor_boot-${build%.*}.img"
          VENDOR_KERNEL_BOOT_IMG_NAME="vendor_kernel_boot-${build%.*}.img"
          RECOVERY_IMG_NAME="recovery-${build%.*}.img"
          RECOVERY_ZIP_NAME="recovery-IMG-${build%.*}.zip"

          if [ "$RECOVERY_NEEDS_SUPPORT_IMG" = true ]; then
            # Copy the files specified by the flags
            if [ "$RECOVERY_NEEDS_BOOT_IMG" = true ]; then
              cp -a $IMG_OUT/boot.img "$BOOT_IMG_NAME"
              sha256sum "$BOOT_IMG_NAME" >"$BOOT_IMG_NAME.sha256sum"
            fi
            if [ "$RECOVERY_NEEDS_DTBO_IMG" = true ]; then
              cp -a $IMG_OUT/dtbo.img "$DTBO_IMG_NAME"
              sha256sum "$DTBO_IMG_NAME" >"$DTBO_IMG_NAME.sha256sum"
            fi
            if [ "$RECOVERY_NEEDS_VBMETA_IMG" = true ]; then
              cp -a $IMG_OUT/vbmeta.img "$VBMETA_IMG_NAME"
              sha256sum "$VBMETA_IMG_NAME" >"$VBMETA_IMG_NAME.sha256sum"
            fi
            if [ "$RECOVERY_NEEDS_VENDOR_BOOT_IMG" = true ]; then
              cp -a $IMG_OUT/vendor_boot.img "$VENDOR_BOOT_IMG_NAME"
              sha256sum "$VENDOR_BOOT_IMG_NAME" >"$VENDOR_BOOT_IMG_NAME.sha256sum"
            fi
            if [ "$RECOVERY_NEEDS_SUPER_IMG" = true ]; then
              cp -a $IMG_OUT/super_empty.img "$SUPER_EMPTY_IMG_NAME"
              sha256sum "$SUPER_EMPTY_IMG_NAME" >"$SUPER_EMPTY_IMG_NAME.sha256sum"
            fi
            if [ "$RECOVERY_NEEDS_VENDOR_KERNEL_BOOT_IMG" = true ]; then
              cp -a $IMG_OUT/vendor_kernel_boot.img "$VENDOR_KERNEL_BOOT_IMG_NAME"
              sha256sum "$VENDOR_KERNEL_BOOT_IMG_NAME" >"$VENDOR_KERNEL_BOOT_IMG_NAME.sha256sum"
            fi

            if [ "$VBOOT_IS_RECOVERY" = true ]; then
              cp -a $IMG_OUT/vendor_boot.img "$RECOVERY_IMG_NAME"
            elif [ -f "$IMG_OUT/recovery.img" ]; then
              cp -a $IMG_OUT/recovery.img "$RECOVERY_IMG_NAME"
            else
              cp -a $IMG_OUT/boot.img "$RECOVERY_IMG_NAME"
            fi

            # Generate SHA256 checksum for RECOVERY_IMG_NAME
            sha256sum "$RECOVERY_IMG_NAME" >"$RECOVERY_IMG_NAME.sha256sum"

            # Conditionally include files in zip command based on flags
            files_to_zip=("$RECOVERY_IMG_NAME" "$RECOVERY_IMG_NAME.sha256sum")

            if [ "$RECOVERY_NEEDS_BOOT_IMG" = true ]; then
              files_to_zip+=("$BOOT_IMG_NAME")
              files_to_zip+=("$BOOT_IMG_NAME.sha256sum")
            fi
            if [ "$RECOVERY_NEEDS_DTBO_IMG" = true ]; then
              files_to_zip+=("$DTBO_IMG_NAME")
              files_to_zip+=("$DTBO_IMG_NAME.sha256sum")
            fi
            if [ "$RECOVERY_NEEDS_VBMETA_IMG" = true ]; then
              files_to_zip+=("$VBMETA_IMG_NAME")
              files_to_zip+=("$VBMETA_IMG_NAME.sha256sum")
            fi
            if [ "$RECOVERY_NEEDS_VENDOR_BOOT_IMG" = true ]; then
              files_to_zip+=("$VENDOR_BOOT_IMG_NAME")
              files_to_zip+=("$VENDOR_BOOT_IMG_NAME.sha256sum")
            fi
            if [ "$RECOVERY_NEEDS_SUPER_IMG" = true ]; then
              files_to_zip+=("$SUPER_EMPTY_IMG_NAME")
              files_to_zip+=("$SUPER_EMPTY_IMG_NAME.sha256sum")
            fi
            if [ "$RECOVERY_NEEDS_VENDOR_KERNEL_BOOT_IMG" = true ]; then
              files_to_zip+=("$VENDOR_KERNEL_BOOT_IMG_NAME")
              files_to_zip+=("$VENDOR_KERNEL_BOOT_IMG_NAME.sha256sum")
            fi
            # Zip command
            zip "$RECOVERY_ZIP_NAME" "${files_to_zip[@]}"
            sha256sum "$RECOVERY_ZIP_NAME" >"$RECOVERY_ZIP_NAME.sha256sum"
            mv "$RECOVERY_ZIP_NAME"* "$ZIP_DIR/$zipsubdir/"
          else
            if [ "$VBOOT_IS_RECOVERY" = true ]; then
              cp -a $IMG_OUT/vendor_boot.img "$RECOVERY_IMG_NAME"
            elif [ -f "$IMG_OUT/recovery.img" ]; then
              cp -a $IMG_OUT/recovery.img "$RECOVERY_IMG_NAME"
            else
              cp -a $IMG_OUT/boot.img "$RECOVERY_IMG_NAME"
            fi

            # Calculate sha256sum and move file
            sha256sum "$RECOVERY_IMG_NAME" >"$RECOVERY_IMG_NAME.sha256sum"
            mv "$RECOVERY_IMG_NAME"* "$ZIP_DIR/$zipsubdir/"
          fi
        fi
      done

      #with only systemimage, we don't have a e-*.zip
      if [ "${BUILD_ONLY_SYSTEMIMAGE}" = true ]; then
        build=e-$(grep lineage.version system/build.prop | sed s/#.*// | sed s/.*=// | tr -d \n).zip
        if [ "$BACKUP_INTERMEDIATE_SYSTEM_IMG" = true ]; then
          mv $IMG_OUT/system.img ./
          zip "$ZIP_DIR/$zipsubdir/IMG-$build" system.img
          cd $ZIP_DIR/$zipsubdir
          sha256sum "IMG-$build" >"IMG-$build.sha256sum"
          md5sum "IMG-$build" >"IMG-$build.md5sum"
          cd "${OUT}" || return 1
        fi
      fi

      if [ "$IS_EMULATOR" = true -a "$BACKUP_EMULATOR" = true ]; then
        EMULATOR_ARCHIVE="IMG-e-$(grep lineage.version system/build.prop | sed s/#.*// | sed s/.*=// | tr -d '\n').zip"
        if [ "$android_version_major" -gt "13" ]; then
          mv sdk-repo-linux-system-images.zip "$ZIP_DIR/$zipsubdir/$EMULATOR_ARCHIVE"
        else
          mv ../../../host/linux-x86/sdk_addon/*-img.zip "$ZIP_DIR/$zipsubdir/$EMULATOR_ARCHIVE"
        fi
        pushd "$ZIP_DIR/$zipsubdir"
        sha256sum "$EMULATOR_ARCHIVE" >"$EMULATOR_ARCHIVE.sha256sum"
        popd
      fi

      cd "$source_dir" || return 1
      echo ">> [$(date)] backup manifest for ${DEVICE}"
      repo manifest -r -o "$ZIP_DIR/$zipsubdir"/${build%???}xml
      build_successful=true
    else
      echo ">> [$(date)] Failed build for ${DEVICE}"
    fi

    clean_up
  fi

fi

exit_script
